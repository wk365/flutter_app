import 'package:flutter/material.dart';
import 'package:bot_toast/bot_toast.dart';

//redux
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:flutter_app/reducers.dart';

//page
import 'package:flutter_app/pages/splash.dart';
import 'package:flutter_app/pages/home/root_page.dart';
import 'package:flutter_app/pages/match_up.dart';
import 'package:flutter_app/pages/list.dart';
import 'package:flutter_app/pages/hero.dart';
import 'package:flutter_app/top_screen.dart';

import 'package:flutter_app/utils/utils.dart';

void main() {
  final store = Store<AppState>(mainReducer,
      initialState: new AppState(
        homePage: new HomePageState(),
        main: new MainPageState(),
        auth: new AuthState(),
      ));

  runApp(new MyApp(store));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  MyApp(this.store);

  initUserStatus() async {
    String storageTicket;
    Map storageUser;
    storageTicket = await Utils.getLocaleData('Ticket');
    storageUser = await Utils.getLocaleData('User');
    //登录
    Utils.login(this.store, {'Ticket': storageTicket, 'User': storageUser});
  }

  @override
  Widget build(BuildContext context) {
    initUserStatus();
    return StoreProvider<AppState>(
      store: store,
      child: BotToastInit(
          child: new MaterialApp(
        navigatorObservers: [BotToastNavigatorObserver()],
        theme: new ThemeData(
          primarySwatch: Colors.blue,
        ),
        initialRoute: '/splash',
        routes: {
          '/splash': (BuildContext context) => Splash(),
          '/home': (BuildContext context) => RootPage(),
          '/matchup': (BuildContext context) =>
              MatchUp(),
          '/list': (BuildContext context) => BasicPage('aa'),
          '/TopScreen': (BuildContext context) => TopScreen(),
          '/hero': (BuildContext context) => HeroSceen(),
        },
        debugShowCheckedModeBanner: false,
      )),
    );
  }
}
