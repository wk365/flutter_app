import 'package:flutter/material.dart';

//redux
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:flutter_app/reducers.dart';

import 'package:flutter_app/pages/login.dart';

import 'package:flutter_app/utils/utils.dart';
import 'dart:async' as Async;

class MyPage extends StatefulWidget {
  @override
  _MyPageState createState() => _MyPageState();
}

class _MyPageState extends State<MyPage> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  void _toLogin(BuildContext context) async {
    final result = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => LoginPage())
    );

    print(result);

    return result;
  }

  //我的用户信息
  Widget _buildStack(store, context) => Stack(
        alignment: const Alignment(0, 0),
        children: [
          new Image.asset(
            'lib/images/userCenterBg.png',
            fit: BoxFit.fill,
          ),
          FlatButton(
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Column(children: [
                CircleAvatar(
                  backgroundImage: AssetImage('lib/images/defaultUser.png'),
                  radius: 35,
                ),
                Padding(
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                    child: Text(
                        store.state.auth.isLogin
                            ? store.state.auth.User['LoginName']
                            : '请登陆',
                        style: TextStyle(fontSize: 16.0, color: Colors.white)))
              ]),
            ),
            onPressed: () async {
              if (!store.state.auth.isLogin) {
                _toLogin(context);
              }
            },
          ),
        ],
      );

  Widget build(BuildContext context) {
    return StoreConnector<AppState, Store<AppState>>(
        converter: (store) => store,
        builder: (context, store) {
          return Scaffold(
              backgroundColor: Color(0xFFe1e1e1),
              body: Center(
                  child: ListView(children: [
                Container(
                  child: _buildStack(store, context),
                ),
                    (store.state.auth.isLogin ? FlatButton(
                    child: Text('退出'),
                    onPressed: () async {
                      //退出
                      await Utils.logout(StoreProvider.of<AppState>(context));
                      
                      _toLogin(context);
                    },
                  ) :Container()),
              ])));
        });
  }
}
