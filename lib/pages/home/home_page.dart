import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'dart:async' as Async;

//redux
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:flutter_app/reducers.dart';

import 'package:flutter_app/utils/utils.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  List<Widget> imageList = List();

  @override
  void initState() {
    super.initState();

    //请求banner
    getBanner();
  }

  //  @override
  getBanner() async {
    //手动延迟0.25秒
    await new Async.Future.delayed(new Duration(milliseconds: 250));
    //加载真实banner数据
    var response  = await Utils.request().get('/b2bbanner');
    var res = response.data['data'];

    //正则替换\符号
    res['Banner'] = res['Banner'].replaceAll('\\', '\/');
    res['Banner2'] = res['Banner2'].replaceAll('\\', '\/');

    List<Widget> tempBannerWidget = List();
    tempBannerWidget.add(Image.network(
      Utils.domain + res['Banner'],
      fit: BoxFit.fill,
    ));
    tempBannerWidget.add(Image.network(
      Utils.domain + res['Banner2'],
      fit: BoxFit.fill,
    ));

    //发起action
    StoreProvider.of<AppState>(context)
        .dispatch(new SetBannerAction(banners: tempBannerWidget));
  }

  @override
  Widget build(BuildContext context) {
    
    return StoreConnector<AppState, Store<AppState>>(
        converter: (store) => store,
        builder: (context, store) {
          return Scaffold(
              backgroundColor: Color(0xFFffffff),
              appBar: new AppBar(
                title: Text('联亚国际'),
                centerTitle: true,
              ),
              body: ListView(children: [
                Container(
                    width: MediaQuery.of(context).size.width,
                    height: (MediaQuery.of(context).size.width * 650) /
                        1920, //按比例计算banner高度
                    child: new Swiper(
                      itemBuilder: (BuildContext context, int index) {
                        if (store.state.homePage.banners == null) {
                          return new Image.asset(
                            'lib/images/banner.png',
                            fit: BoxFit.fill,
                          );
                        } else {
                          return (store.state.homePage.banners[index]);
                        }
                      },
                      itemCount: store.state.homePage.banners == null
                          ? 1
                          : store.state.homePage.banners.length,
                      pagination: new SwiperPagination(),
                      onTap: (index) {
//                        debugPrint("点击了第:$index个");
                      },
                    )),
                Container(
                  child: FlatButton(
                    child: Text("go TopScreen"),
                    onPressed: () {
                      Navigator.pushNamed(context, '/TopScreen');
                    },
                  ),
                ),
                Container(
                  child: FlatButton(
                    child: Text("go hero"),
                    onPressed: () {
                      Navigator.pushNamed(context, '/list');
                    },
                  ),
                ),
              ]),
              drawer: Drawer(
                child: ListView(
                  padding: EdgeInsets.zero,
                  children: [
                    DrawerHeader(
                      child: Row(children: [
                        Icon(Icons.language, size: 22.0),
                        Text('切换语言')
                      ]),
                    ),
                    ListTile(
                      title: Text('中文'),
                    ),
                    ListTile(
                      title: Text('English'),
                    ),
                  ],
                ),
              ));
        });
  }
}
