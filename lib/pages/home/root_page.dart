import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

//redux
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:flutter_app/reducers.dart';

import 'package:flutter_app/pages/home/home_page.dart';
import 'package:flutter_app/pages/home/my_page.dart';
import 'package:flutter_app/pages/widget/routeParams.dart';

class RootPage extends StatefulWidget {
  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  int _currentIndex = 0;
  List<Widget> list;
  PageController _pageController;

  //  退出确认
  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text('确定退出程序吗?'),
              actions: <Widget>[
                FlatButton(
                  child: Text('暂不'),
                  onPressed: () => Navigator.pop(context, false),
                ),
                FlatButton(
                    child: Text('确定'),
                    onPressed: () {
                      Navigator.pop(context, true);
                    }),
              ],
            ));
  }

  @override
  void initState() {
    list = <Widget>[
      HomePage(),
      Container(),
      MyPage(),
    ];
    super.initState();
    this._pageController =
        PageController(initialPage: this._currentIndex, keepPage: true);
    
//    print('init');
  }

  @override
  Widget build(BuildContext context) {
//    print('build');

    return StoreConnector<AppState, Store<AppState>>(
        converter: (store) =>
            store, // 构建器，第二个参数 store 就是上一个 converter 函数返回的 store
        builder: (context, store) {
          return WillPopScope(
              onWillPop: _onBackPressed,
              child: Scaffold(
                  body: PageView(
                      children: list,
                      controller: _pageController,
                      physics: NeverScrollableScrollPhysics()),
                  bottomNavigationBar: BottomNavigationBar(
                    items: [
                      BottomNavigationBarItem(
                          icon: Icon(
                            Icons.home,
                          ),
                          title: Text(
                            '主页',
                          )),
                      BottomNavigationBarItem(
                          icon: Icon(
                            Icons.casino,
                          ),
                          title: Text(
                            'Match Up',
                          )),
                      BottomNavigationBarItem(
                          icon: Icon(
                            Icons.account_circle,
                          ),
                          title: Text(
                            '我的',
                          )),
                    ],
                    currentIndex: _currentIndex,
                    onTap: (int index) {
                      if (index == 2) {
                        //设置状态栏蓝色
                        SystemChrome.setSystemUIOverlayStyle(
                            SystemUiOverlayStyle(
                                statusBarColor: Colors.blue,
                                statusBarIconBrightness: Brightness.light));
                      } else {
                        //重置状态栏
                        /*SystemChrome.setSystemUIOverlayStyle(
                          SystemUiOverlayStyle(
                            statusBarColor: Colors.transparent, statusBarIconBrightness: Brightness
                            .light));*/
                      }

                      if (index != 1) {
                        setState(() {
                          _currentIndex = index;
                          _pageController.jumpToPage(index);
                        });
                      } else {
                        Navigator.pushNamed(context, '/matchup');
                      }
                    },
                    type: BottomNavigationBarType.fixed,
                  )));
        });
  }
}
