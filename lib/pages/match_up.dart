import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

//redux
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_app/reducers.dart';

//import 'dart:convert';
//import 'package:flutter_app/utils/utils.dart';

class MatchUp extends StatefulWidget {
	@override
	_MatchUpState createState() => _MatchUpState();
}

class _MatchUpState extends State<MatchUp> {
	void onPressAdd() {
		print('press');
	}
	
	Widget build(BuildContext context) {
		return Scaffold(
			backgroundColor: Color(0xFFcccccc),
			appBar: AppBar(
				title: Text('MatchUp'),
				centerTitle: true,
			),
			body: new WebviewScaffold(
				url: "http://b2b.nigeriatex.com/mobile/matchupExpo",
				withZoom: true,
				withLocalStorage: true,
				hidden: true,
				initialChild: Container(
					color: Colors.white30,
					child: const Center(
						child: CircularProgressIndicator(
							backgroundColor: Colors.grey,
							valueColor: AlwaysStoppedAnimation(Colors.blue),
						),
					),
				),
			));
	}
}
