import 'package:flutter/material.dart';
import 'package:bot_toast/bot_toast.dart';

//redux
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_app/reducers.dart';

import 'package:flutter_app/utils/utils.dart';

class LoginPage extends StatefulWidget {
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  String _account;
  String _pwd;
  bool isLogin;
  GlobalKey _formKey = new GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  void login() async {
    CancelFunc cancelLoading;
    if ((_formKey.currentState as FormState).validate()) {
      cancelLoading = BotToast.showLoading(align: Alignment.center);
      var response = await Utils.request().post('/WebUser/Login',
          data: {'LoginName': '13770974621', 'LoginPass': '111111'});
      cancelLoading();
      var res = response.data['data'];

      if (response.data['code'] == 200) {
        //登录
        Utils.login(StoreProvider.of<AppState>(context), {'Ticket': res['Ticket'], 'User': res['User']});

        Navigator.pop(context, {'data': '来自登陆页'});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("登录"),
      ),
      body: new Form(
          key: _formKey,
          onChanged: () {
            //            print("changed");
          },
          /*onWillPop: () async {
            return true;
          },*/
          child: new Padding(
            padding: new EdgeInsets.all(10.0),
            child: new Column(
              children: [
                new TextFormField(
                  decoration: new InputDecoration(labelText: "请输入手机号"),
                  onSaved: (String value) {
                    _account = value;
                  },
                  validator: (String value) => value.isEmpty ? "请输入账号" : null,
                ),
                new TextFormField(
                    decoration: new InputDecoration(labelText: "请输入密码"),
                    onSaved: (String value) => _pwd = value,
                    validator: (String value) =>
                        value.isEmpty ? "请输入密码" : null),
                new FormField(builder: (FormFieldState s) {
                  return new Center(
                      child: new RaisedButton(
                    color: Colors.blue,
                    onPressed: login,
                    child: new Text("登录"),
                    highlightColor: Colors.blue[700],
                    colorBrightness: Brightness.dark,
                    splashColor: Colors.grey,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0)),
                  ));
                })
              ],
            ),
          )),
    );
  }
}
