import 'package:flutter/material.dart';

/// 管理主页状态
class HomePageState {
  List<Widget> banners;
  int testIndex;

  HomePageState({this.banners, this.testIndex: 12});
}

/// 这个类用来管理登录状态
class AuthState {
  bool isLogin; //是否登录
  String Ticket;
  Map User;

  AuthState({this.isLogin: false, this.Ticket, this.User});
}

/// 管理主页状态
class MainPageState {
  int counter;

  MainPageState({this.counter: 12});

//  @override
//  String toString() {
//    return "{counter:$counter}";
//  }
}

/// 应用程序状态
class AppState {
  AuthState auth; //登录
  MainPageState main; //主页
  HomePageState homePage;

  AppState({this.auth, this.main, this.homePage});
}

/**********************************************/

enum Actions1 { InitStatus, Increase, Login, LoginSuccess, LogoutSuccess, SetBannerSuccess }

/// 定义所有action的基类
class Action {
  final Actions1 type;

  Action({this.type});
}

/// 定义Login成功action
class InitStatus extends Action {
  InitStatus()
      : super(type: Actions1.LoginSuccess);
}

/// 定义Login成功action
class LoginSuccessAction extends Action {
  final String Ticket;
  final Map User;

  LoginSuccessAction({this.Ticket, this.User})
      : super(type: Actions1.LoginSuccess);
}

//首页banner
class SetBannerAction extends Action {
  List<Widget> banners;

  SetBannerAction({this.banners}) : super(type: Actions1.SetBannerSuccess);
}

AppState mainReducer(AppState state, dynamic action) {
  if (action is InitStatus) {
    state.auth.User = {};
  }
  
  if (Actions1.Increase == action) {
    state.main.counter += 1;
  }

  if (Actions1.LogoutSuccess == action) {
    state.auth.isLogin = false;
    state.auth.Ticket = null;
    state.auth.User = null;
  }

  if (action is LoginSuccessAction) {
    state.auth.isLogin = true;
    state.auth.Ticket = action.Ticket;
    state.auth.User = action.User;
  }

  if (action is SetBannerAction) {
    state.homePage.banners = action.banners;
  }

  return state;
}

//loggingMiddleware(Store<AppState> store, action, NextDispatcher next) {
//  print('${new DateTime.now()}: $action');
//
//  next(action);
//}
